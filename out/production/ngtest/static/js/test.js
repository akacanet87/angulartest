/* #1 */
/*var app = angular.module('myApp', []);
app.controller('myCtrl', function ($scope) {
    $scope.name = "John Doe";
});*/

/* #2 */
/*var app = angular.module('myApp', []);
app.controller('myCtrl', function ($scope){
    $scope.firstName = "John";
    $scope.lastName = "Doe";
});*/

/* #6 */
/*angular.module('myApp', []).controller('namesCtrl', function($scope){
    $scope.names = [
        {name:'Jani', country:'Norway'},
        {name:'Suzan', country:'Norway'},
        {name:'Brian', country:'North Ireland'},
        {name:'Sia', country:'Korea'},
        {name:'Jiho', country:'Korea'},
        {name:'John', country:'England'},
        {name:'Lora', country:'England'},
        {name:'Ljungberg', country:'Swiss'},
        {name:'Lihtenstein', country:'Swiss'},
        {name:'Sona', country:'Sweden'},
        {name:'Cesc', country:'Spain'},
        {name:'Carl', country:'Canada'},
        {name:'Rakit', country:'Croatia'},
        {name:'Rodrez', country:'Columbia'},
        {name:'Jermain', country:'Cuba'}
    ];
    $scope.orderByMe = function(x){
        $scope.myOrderBy = x;
    }
});*/

/* #7 */
/*var app = angular.module('myApp', []);
app.filter('myFormat', function(){
    return function(x){
        var i, c, txt = "";
        for(i = 0; i < x.length ; i++){
            c = x[i];
            if(i%2 === 0){
                c = c.toUpperCase();
            }
            txt += c;
        }
        return txt;
    }
});
app.controller('namesCtrl', function($scope){
    $scope.names = [
        {name:'Jani', country:'Norway'},
        {name:'Suzan', country:'Norway'},
        {name:'Brian', country:'North Ireland'},
        {name:'Sia', country:'Korea'},
        {name:'Jiho', country:'Korea'},
        {name:'John', country:'England'},
        {name:'Lora', country:'England'},
        {name:'Ljungberg', country:'Swiss'},
        {name:'Lihtenstein', country:'Swiss'},
        {name:'Sona', country:'Sweden'},
        {name:'Cesc', country:'Spain'},
        {name:'Carl', country:'Canada'},
        {name:'Rakit', country:'Croatia'},
        {name:'Rodrez', country:'Columbia'},
        {name:'Jermain', country:'Cuba'}
    ];
});*/

/* #8 */
/*var app = angular.module('myApp', []);
app.controller('myCtrl', function($scope, $http){
    $http.get("welcome.htm").then(function(response){
    $scope.myWelcome = response.data;
    });
});*/

/* #9 */
/*var app = angular.module('myApp', []);
app.controller('myCtrl', function($scope, $timeout){
    $scope.myHeader = "Hello World";
    $timeout(function(){
        $scope.myHeader = "How are you today?";
    }, 2500);
});*/

/* #10 */
/*var app = angular.module('myApp', []);
app.controller('myCtrl', function($scope, $interval){
    $scope.theTime = new Date().toLocaleTimeString();
    $interval(function(){
        $scope.theTime = new Date().toLocaleTimeString();
    }, 1000);
});*/

/* #11 */
/*
var app = angular.module('myApp', []);

//  service는 한번만 호출되나봄
app.service('hexafy', function(){
    this.myFunc = function(x){
        console.log(x + ', ' + x.toString(16));
        return x.toString(16);
    }
});
app.controller('myCtrl', function($scope, hexafy){
    window.addEventListener('keydown', function(){
        if($scope.decNum !== undefined && $scope.decNum !== null){
            $scope.hex = hexafy.myFunc($scope.decNum);
        }else{
            $scope.hex = 0;
        }
    });
});*/

/* #13 */
/*
//  filter는 매번 갱신되고 service는 한번 호출 되는 듯
var app = angular.module('myApp', []);
app.service('hexafy', function(){
    this.myFunc = function(x){
        return x.toString(16);
    }
});
app.filter('myFormat', ['hexafy', function(hexafy){
    return function (x) {
        if(x !== undefined && x !== null){
            return hexafy.myFunc(x);
        }else{
            return 0;
        }
    }
}]);*/

/* #14 */
/*var app = angular.module('myApp', []);
app.controller('myCtrl', function($scope, $http){
    $http.get('https://www.w3schools.com/angular/customers.php')
        .then(function(response){
            $scope.myData = response.data.records;
        });
});*/

/* #15 */
var app = angular.module('myApp', []);
app.controller('myCtrl', function($scope, $http){
    $http.get('https://www.w3schools.com/angular/customers.php')
        .then(function(response){
            $scope.names = response.data.records;
        });
});