var curPage = 1;
var app = angular.module('boardApp', ['ngRoute']);
app.config(function ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'board-list.html'
        })
        .when('/write', {
            templateUrl: 'write.html'
        })
        .when('/detail/:contentNum', {
            templateUrl: 'detail.html'
        });
});
app.factory('PagerService', function () {
    var service = {};

    service.GetPager = GetPager;

    return service;

    // service implementation
    function GetPager(totalItems, currentPage, pageSize) {
        // default to first page
        currentPage = currentPage || 1;

        // default page size is 10
        pageSize = pageSize || 10;

        // calculate total pages
        var totalPages = Math.ceil(totalItems / pageSize);

        var startPage, endPage;
        if (totalPages <= 10) {
            // less than 10 total pages so show all
            startPage = 1;
            endPage = totalPages;
        } else {
            // more than 10 total pages so calculate start and end pages
            if (currentPage <= 6) {
                startPage = 1;
                endPage = 10;
            } else if (currentPage + 4 >= totalPages) {
                startPage = totalPages - 9;
                endPage = totalPages;
            } else {
                startPage = currentPage - 5;
                endPage = currentPage + 4;
            }
        }

        // calculate start and end item indexes
        var startIndex = (currentPage - 1) * pageSize;
        var endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

        // create an array of pages to ng-repeat in the pager control
        var pages = _.range(startPage, endPage + 1);

        // return object with all pager properties required by the view
        return {
            totalItems: totalItems,
            currentPage: currentPage,
            pageSize: pageSize,
            totalPages: totalPages,
            startPage: startPage,
            endPage: endPage,
            startIndex: startIndex,
            endIndex: endIndex,
            pages: pages
        };
    }
});
app.factory('Contents', function () {
    var contents = [
        {
            num: 12321,
            writer: 'tim',
            password: 'a',
            title: 'Cheer up',
            content: 'Let\'s get drunk and drive around',
            date: '08.04 05:31',
            count: 0
        },
        {
            num: 12322,
            writer: 'typhoon',
            password: 'a',
            title: 'Finally',
            content: 'Now we got one more night',
            date: '08.03 06:55',
            count: 0
        },
        {
            num: 12323,
            writer: 'king',
            password: 'a',
            title: 'Tonight',
            content: 'We\'ll cry',
            date: '08.03 07:14',
            count: 0
        },
        {
            num: 12324,
            writer: 'aaa',
            password: 'a',
            title: 'This is for you',
            content: 'It\'s so good to be by your side',
            date: '08.03 07:58',
            count: 0
        },
        {
            num: 12325,
            writer: 'anold',
            password: 'a',
            title: 'You know what',
            content: 'Tonight we\'ll change our lives',
            date: '08.03 08:48',
            count: 0
        },
        {
            num: 12326,
            writer: 'bold',
            password: 'a',
            title: 'Try this',
            content: 'Everything that kills me makes me feel alive',
            date: '08.03 09:27',
            count: 0
        },
        {
            num: 12327,
            writer: 'old',
            password: 'a',
            title: 'hahaha',
            content: 'I couldnt lie',
            date: '08.03 10:10',
            count: 0
        },
        {
            num: 12328,
            writer: 'fbi',
            password: 'a',
            title: 'Choose one',
            content: 'I could lie',
            date: '08.03 12:55',
            count: 0
        },
        {
            num: 12329,
            writer: 'kas',
            password: 'a',
            title: 'This picture',
            content: 'I couldnt lie',
            date: '08.03 15:32',
            count: 0
        },
        {
            num: 12330,
            writer: 'ttt',
            password: 'a',
            title: 'Iphone or Galaxy?',
            content: 'Doing the right thing',
            date: '08.03 18:16',
            count: 0
        },
        {
            num: 12331,
            writer: 'set',
            password: 'a',
            title: 'Anyone try',
            content: 'Feel something so wrong',
            date: '08.03 21:07',
            count: 0
        },
        {
            num: 12332,
            writer: '5423',
            password: 'a',
            title: 'Delete this',
            content: 'Doing the wrong thing',
            date: '08.03 21:59',
            count: 0
        },
        {
            num: 12333,
            writer: 'taken',
            password: 'a',
            title: 'Sure, you can',
            content: 'Feel something so right',
            date: '08.03 22:08',
            count: 0
        },
        {
            num: 12334,
            writer: 'espada',
            password: 'a',
            title: 'Just..',
            content: 'I\'m just doing what we told',
            date: '08.03 22:15',
            count: 0
        },
        {
            num: 12335,
            writer: 'ken',
            password: 'a',
            title: 'Show me',
            content: 'I dont think the world is sold',
            date: '08.03 22:47',
            count: 0
        },
        {
            num: 12336,
            writer: 'aa',
            password: 'a',
            title: 'Who is nobody?',
            content: 'But I\'m not that bold',
            date: '08.03 23:16',
            count: 0
        },
        {
            num: 12337,
            writer: 'hello',
            password: 'a',
            title: 'Never say goodbye',
            content: 'Old but I\'m not that old young',
            date: '08.03 23:35',
            count: 0
        },
        {
            num: 12338,
            writer: 'nobody',
            password: 'a',
            title: 'I miss you',
            content: 'Seek it out and you shell find',
            date: '08.03 23:47',
            count: 0
        },
        {
            num: 12339,
            writer: 'hahaha',
            password: 'a',
            title: 'Break free',
            content: 'In my faces flashing sign',
            date: '08.04 00:07',
            count: 0
        },
        {
            num: 12340,
            writer: 'asdf',
            password: 'a',
            title: 'Imagine that',
            content: 'Swing my heart across the line',
            date: '08.04 00:35',
            count: 0
        },
        {
            num: 12341,
            writer: 'a',
            password: 'a',
            title: 'One word',
            content: 'Like a swinging vine',
            date: '08.04 00:47',
            count: 0
        },
        {
            num: 12342,
            writer: 'joe',
            password: 'a',
            title: 'Say hello to me',
            content: 'I see this life',
            date: '08.04 01:12',
            count: 0
        },
        {
            num: 12343,
            writer: 'king',
            password: 'a',
            title: 'I wanna sell my treasure',
            content: 'Said no more counting dollars, we\'ll be counting stars',
            date: '08.04 01:21',
            count: 0
        },
        {
            num: 12344,
            writer: 'who',
            password: 'a',
            title: 'Really?',
            content: 'Babe I\'ve been praying hard',
            date: '08.04 06:24',
            count: 0
        },
        {
            num: 12345,
            writer: 'jane',
            password: 'a',
            title: 'And so on',
            content: 'Dreaming about the things that we could be',
            date: '08.04 07:46',
            count: 0
        },
        {
            num: 12346,
            writer: 'abc',
            password: 'a',
            title: 'Now..',
            content: 'Lately I\'ve been losing sleep',
            date: '08.04 08:12',
            count: 0
        },
        {
            num: 12347,
            writer: 'abb',
            password: 'a',
            title: 'So what',
            content: 'Hey dudes, It\'s party time~!!',
            date: '08.04 08:33',
            count: 0
        },
        {
            num: 12348,
            writer: 'aba',
            password: 'a',
            title: 'Can you believe this?',
            content: 'Let\'s have a show time',
            date: '08.04 09:02',
            count: 0
        },
        {
            num: 12349,
            writer: 'ddd',
            password: 'a',
            title: 'What?',
            content: 'Does anybody count this?',
            date: '08.04 10:56',
            count: 0
        },
        {
            num: 12350,
            writer: 'ccc',
            password: 'a',
            title: 'Go away',
            content: 'I am the king',
            date: '08.04 11:33',
            count: 0
        },
        {
            num: 12351,
            writer: 'bbb',
            password: 'a',
            title: 'Hi',
            content: 'Did you see this?',
            date: '08.04 12:42',
            count: 0
        },
        {
            num: 12352,
            writer: 'aaa',
            password: 'a',
            title: 'Hello',
            content: 'what are you looking for?',
            date: '08.04 16:18',
            count: 0
        },
    ];
    return contents;
});
app.controller('boardListCtrl', function ($scope, $location, Contents, PagerService) {
    var vm = this;

    vm.dummyItems = Contents; // dummy array of items to be paged
    vm.pager = {};
    vm.setPage = setPage;

    initController();

    function initController() {
        // initialize to page 1
        vm.setPage(curPage);
    }

    function setPage(page) {
        if (page < 1 || page > vm.pager.totalPages) {
            return;
        }

        // get pager object from service
        vm.pager = PagerService.GetPager(vm.dummyItems.length, page, $scope.pageLimit);

        // get current page of items
        vm.items = vm.dummyItems.slice(vm.pager.startIndex, vm.pager.endIndex + 1);
    }
    $scope.optionChanged = function(){initController()};
    $scope.contents = Contents;
    $scope.contents = $scope.contents.sort(function (a, b) {
        return b['num'] - a['num'];
    });
    $scope.pageLimit = 10;
    $scope.orderByThis = function (content) {
        $scope.contentOrderBy = content
    };
    $scope.mouseOverStyle = function (content) {
        $scope.hoveredRow = content;
    };
    $scope.showDetail = function (content) {
        console.log(content.num);
        $location.url('/detail/' + content.num);
    };
    $scope.writeContent = function () {
        $location.url('/write');
    };
    $scope.deleteContent = function (content) {
        console.log(content.password);
        if (prompt('Enter your password', '') === content.password) {
            if (confirm('Do you really want delete this content?')) {
                for (var i = 0; i < $scope.contents.length; i++) {
                    if ($scope.contents[i].num === content.num) {
                        $scope.contents.splice(i, 1);
                        alert('Delete content successful');
                        $location.url('/');
                    }
                }
            }
        } else {
            alert('You entered wrong password');
        }
    };
});
app.controller('writeCtrl', function ($scope, $location, Contents) {
    $scope.contents = Contents;
    var content = null;
    var contentNum = (Math.max.apply(Math, $scope.contents.map(function (o) {
        return o.num;
    })) + 1);
    $scope.saveContent = function () {
        var date = getDate();
        content = {
            num: contentNum,
            writer: $scope.writer,
            title: $scope.title,
            content: $scope.content,
            password: $scope.password,
            date: date,
            count: 0
        };
        console.log(content);
        $scope.contents.push(content);
        $location.url('/');
    };
    $scope.toList = function(){
        $location.url('/');
    };
});
app.controller('detailCtrl', function ($scope, $routeParams, $location, Contents) {
    $scope.contentNum = $routeParams.contentNum;
    $scope.contents = Contents;
    $scope.isEditMode = false;
    var content = null;
    for (var i = 0; i < $scope.contents.length; i++) {
        if (parseInt($scope.contents[i].num) === parseInt($scope.contentNum)) {
            content = $scope.contents[i];
            $scope.contents[i].count = parseInt($scope.contents[i].count) + 1;
            $scope.title = content.title;
            $scope.content = content.content;
            $scope.writer = content.writer;
        }
    }
    $scope.editContent = function () {
        if ($scope.isEditMode === false) {
            if (prompt('Enter your password', null) === content.password) {
                $scope.isEditMode = true;
                $scope.password = content.password;
            } else {
                alert('You have wrong password');
            }
        }
    };
    $scope.saveContent = function () {
        var date = getDate();
        for (var i = 0; i < $scope.contents.length; i++) {
            if (parseInt($scope.contents[i].num) === parseInt($scope.contentNum)) {
                console.log($scope.contents[i]);
                $scope.contents[i].title = $scope.title;
                $scope.contents[i].content = $scope.content;
                $scope.contents[i].writer = $scope.writer;
                $scope.contents[i].date = date;
            }
        }
        $location.url('/');
    };
    $scope.toList = function(){
        $location.url('/');
    };
});

function getDate() {
    var now = new Date();
    var month = '0' + ( now.getMonth() + 1 );
    var date = '0' + ( now.getDate() );
    var hour = '0' + ( now.getHours() );
    var minute = '0' + ( now.getMinutes() );
    var fullDate = month.substring(month.length - 2, month.length) + '.'
        + date.substring(date.length - 2, date.length) + ' '
        + hour.substring(hour.length - 2, hour.length) + ':'
        + minute.substring(minute.length - 2, minute.length);
    console.log(fullDate);
    return fullDate;
}