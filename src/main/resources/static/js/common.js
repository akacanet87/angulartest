var app = angular.module('boardApp', ['ngRoute']);

app.config(function ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'member.html'
        })
        .when('/admin', {
            templateUrl: 'admin.html'
        });
});

app.factory('Users', function () {
    var users = [
        {
            id: 1,
            name: 'Pope',
            gender: 'Male',
            age: 30,
            email: 'pope@sc.com',
            grade: 1,
            password: 'aaaa',
            admin: true
        },
        {
            id: 2,
            name: 'Philip',
            gender: 'Male',
            age: 22,
            email: 'philip@sc.com',
            grade: 4,
            password: 'aaaa',
            admin: false
        },
        {
            id: 3,
            name: 'Edward',
            gender: 'Male',
            age: 25,
            email: 'edward@sc.com',
            grade: 3,
            password: 'aaaa',
            admin: false
        },
        {
            id: 4,
            name: 'Sara',
            gender: 'Female',
            age: 21,
            email: 'sara@sc.com',
            grade: 4,
            password: 'aaaa',
            admin: false
        },
        {
            id: 5,
            name: 'Noel',
            gender: 'Male',
            age: 29,
            email: 'noel@sc.com',
            grade: 2,
            password: 'aaaa',
            admin: false
        },
        {
            id: 6,
            name: 'Elise',
            gender: 'Female',
            age: 24,
            email: 'elise@sc.com',
            grade: 4,
            password: 'aaaa',
            admin: false
        },
        {
            id: 7,
            name: 'Bob',
            gender: 'Male',
            age: 27,
            email: 'bob@sc.com',
            grade: 2,
            password: 'aaaa',
            admin: false
        },
        {
            id: 8,
            name: 'Shanon',
            gender: 'Female',
            age: 28,
            email: 'shanon@sc.com',
            grade: 3,
            password: 'aaaa',
            admin: false
        },
        {
            id: 9,
            name: 'Carl',
            gender: 'Male',
            age: 23,
            email: 'carl@sc.com',
            grade: 3,
            password: 'aaaa',
            admin: false
        },
        {
            id: 10,
            name: 'Ryan',
            gender: 'Male',
            age: 21,
            email: 'ryan@sc.com',
            grade: 4,
            password: 'aaaa',
            admin: false
        }
    ];
    return users;
});
app.factory('Owner', function () {
    var owner = [];
    return owner;
});

app.controller('mainCtrl', function ($scope, $location, Users, Owner) {
    $scope.users = Users;
    $scope.owner = Owner;
    $scope.showPage = function (url) {
        $location.path(url);
    };
    $scope.hasLogin = false;
    $scope.loginError = false;
    $scope.isAdmin = false;
    $scope.login = function ($valid, email, password) {
        console.log($valid);
        if ($valid) {
            for (var i = 0; i < $scope.users.length; i++) {
                var user = $scope.users[i];
                if (user.email === email && user.password === password) {
                    $scope.owner.push(user);
                    console.log($scope.owner[0]);
                }
            }
            if ($scope.owner[0] === null || $scope.owner[0] === undefined) {
                $scope.hasLogin = false;
                $scope.loginError = true;
                $scope.email = '';
                $scope.password = '';
            } else {
                $scope.hasLogin = true;
                $scope.loginError = false;
                $scope.isAdmin = $scope.owner[0].admin;
                console.log($scope.isAdmin);
                $location.path('/');
            }
        } else {
            $scope.hasLogin = false;
            $scope.loginError = true;
            $scope.email = '';
            $scope.password = '';
        }
    };
});
app.controller('adminCtrl', function ($scope, Users, $location) {
    $scope.users = Users;
    $scope.showNE = false;
    $scope.showPage = function (url) {
        $location.path(url);
    };
    $scope.orderByThis = function (user) {
        $scope.userOrderBy = user;
    };
    $scope.mouseOverStyle = function (user) {
        $scope.hoveredRow = user;
    };
    $scope.saveUser = function ($valid) {
        $scope.gradeError = false;
        $scope.nameError = false;
        $scope.ageError = false;
        $scope.emailError = false;
        $scope.passwordError = false;
        if ($scope.grade === null || $scope.grade === undefined || $scope.grade === '') {
            $scope.gradeError = true;
        } else if ($scope.name === null || $scope.name === undefined || $scope.name === '') {
            $scope.nameError = true;
        } else if ($scope.age === null || $scope.age === undefined || $scope.age === '') {
            $scope.ageError = true;
        } else if ($scope.email === null || $scope.email === undefined || $scope.email === '') {
            $scope.emailError = true;
        } else if($scope.password !== $scope.repeat){
            $scope.passwordError = true;
        } else if ($valid) {
            if ($scope.id !== null && $scope.id !== undefined) {
                for (var i = 0; i < $scope.users.length; i++) {
                    var oldUser = $scope.users[i];
                    if (oldUser.id === $scope.id) {
                        oldUser.grade = $scope.grade;
                        oldUser.name = $scope.name;
                        oldUser.gender = $scope.gender;
                        oldUser.age = $scope.age;
                        oldUser.email = $scope.email;
                        oldUser.password = $scope.password;
                    }
                }
            } else {
                var user = {
                    id: (Math.max.apply(Math, $scope.users.map(function (o) {
                        return o.id;
                    })) + 1),
                    grade: $scope.grade,
                    name: $scope.name,
                    gender: $scope.gender,
                    age: $scope.age,
                    email: $scope.email,
                    password: $scope.password
                };
                console.log(user);
                $scope.users.push(user);
            }
            $scope.id = null;
            $scope.grade = '';
            $scope.name = '';
            $scope.gender = 'Male';
            $scope.age = '';
            $scope.email = '';
            $scope.password = '';
            $scope.repeat = '';
            $scope.admin = '';
            $scope.showNE = false;
        }
    };
    $scope.editUser = function (user) {
        $scope.showNE = true;
        if (user === 0) {
            $scope.newOrEdit = "New Member";
            $scope.id = null;
            $scope.grade = '';
            $scope.name = '';
            $scope.gender = 'Male';
            $scope.age = '';
            $scope.email = '';
            $scope.password = '';
            $scope.repeat = '';
            $scope.admin = '';
        } else {
            $scope.newOrEdit = "Edit Member " + user.name;
            $scope.id = user.id;
            $scope.grade = user.grade;
            $scope.name = user.name;
            $scope.gender = user.gender;
            $scope.age = user.age;
            $scope.email = user.email;
            $scope.password = user.password;
            $scope.admin = false;
        }
    };
    $scope.deleteUser = function (user, $index) {
        if (confirm('정말로 ' + user.name + ' 을(를) 삭제하시겠습니까?')) {
            $scope.users.splice($index, 1);
        }
    };
});
app.controller('memberCtrl', function ($scope, Users, Owner) {
    $scope.users = Users;
    $scope.owner = Owner;
    $scope.orderByThis = function (user) {
        $scope.userOrderBy = user;
    };
    $scope.mouseOverStyle = function (user) {
        $scope.hoveredRow = user;
    };
    $scope.isOwner = function (id) {
        if ($scope.owner[0] !== null && $scope.owner[0] !== undefined) {
            if ($scope.owner[0].id === id) {
                /*console.log($scope.owner[0]);
                 console.log(id);*/
                //  mouseOver 이벤트 발생 시 계속 호출되는 문제
                return true;
            } else {
                return false;
            }
        }
    };
    $scope.editUser = function (user) {
        $scope.showNE = true;
        if (user === 0) {
            $scope.newOrEdit = "New Member";
            $scope.id = null;
            $scope.grade = '';
            $scope.name = '';
            $scope.gender = 'Male';
            $scope.age = '';
            $scope.email = '';
            $scope.password = '';
            $scope.repeat = '';
            $scope.admin = '';
        } else {
            $scope.newOrEdit = "Edit Member " + user.name;
            $scope.id = user.id;
            $scope.grade = user.grade;
            $scope.name = user.name;
            $scope.gender = user.gender;
            $scope.age = user.age;
            $scope.email = user.email;
            $scope.password = user.password;
            $scope.admin = false;
        }
    };
    $scope.saveUser = function ($valid) {
        $scope.passwordError = false;
        $scope.emailError = false;
        if ($scope.password !== $scope.repeat) {
            $scope.passwordError = true;
        } else if ($scope.email === '' || $scope.email === null || $scope.email === undefined) {
            $scope.emailError = true;
        } else if ($valid) {
            if ($scope.id !== null && $scope.id !== undefined) {
                for (var i = 0; i < $scope.users.length; i++) {
                    var oldUser = $scope.users[i];
                    if (oldUser.id === $scope.id) {
                        oldUser.grade = $scope.grade;
                        oldUser.name = $scope.name;
                        oldUser.gender = $scope.gender;
                        oldUser.age = $scope.age;
                        oldUser.email = $scope.email;
                        oldUser.password = $scope.password;
                    }
                }
            } else {
                var user = {
                    id: (Math.max.apply(Math, $scope.users.map(function (o) {
                        return o.id;
                    })) + 1),
                    grade: $scope.grade,
                    name: $scope.name,
                    gender: $scope.gender,
                    age: $scope.age,
                    email: $scope.email,
                    password: $scope.password
                };
                console.log(user);
                $scope.users.push(user);
            }
            $scope.id = null;
            $scope.grade = '';
            $scope.name = '';
            $scope.gender = 'Male';
            $scope.age = '';
            $scope.email = '';
            $scope.password = '';
            $scope.repeat = '';
            $scope.admin = '';
            $scope.showNE = false;
        }
    };
});