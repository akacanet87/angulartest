/* #1 */
/*var app = angular.module('myApp', []);
 app.controller('myCtrl', function ($scope) {
 $scope.name = "John Doe";
 });*/

/* #2 */
/*var app = angular.module('myApp', []);
 app.controller('myCtrl', function ($scope){
 $scope.firstName = "John";
 $scope.lastName = "Doe";
 });*/

/* #6 */
/*angular.module('myApp', []).controller('namesCtrl', function($scope){
 $scope.names = [
 {name:'Jani', country:'Norway'},
 {name:'Suzan', country:'Norway'},
 {name:'Brian', country:'North Ireland'},
 {name:'Sia', country:'Korea'},
 {name:'Jiho', country:'Korea'},
 {name:'John', country:'England'},
 {name:'Lora', country:'England'},
 {name:'Ljungberg', country:'Swiss'},
 {name:'Lihtenstein', country:'Swiss'},
 {name:'Sona', country:'Sweden'},
 {name:'Cesc', country:'Spain'},
 {name:'Carl', country:'Canada'},
 {name:'Rakit', country:'Croatia'},
 {name:'Rodrez', country:'Columbia'},
 {name:'Jermain', country:'Cuba'}
 ];
 $scope.orderByMe = function(x){
 $scope.myOrderBy = x;
 }
 });*/

/* #7 */
/*var app = angular.module('myApp', []);
 app.filter('myFormat', function(){
 return function(x){
 var i, c, txt = "";
 for(i = 0; i < x.length ; i++){
 c = x[i];
 if(i%2 === 0){
 c = c.toUpperCase();
 }
 txt += c;
 }
 return txt;
 }
 });
 app.controller('namesCtrl', function($scope){
 $scope.names = [
 {name:'Jani', country:'Norway'},
 {name:'Suzan', country:'Norway'},
 {name:'Brian', country:'North Ireland'},
 {name:'Sia', country:'Korea'},
 {name:'Jiho', country:'Korea'},
 {name:'John', country:'England'},
 {name:'Lora', country:'England'},
 {name:'Ljungberg', country:'Swiss'},
 {name:'Lihtenstein', country:'Swiss'},
 {name:'Sona', country:'Sweden'},
 {name:'Cesc', country:'Spain'},
 {name:'Carl', country:'Canada'},
 {name:'Rakit', country:'Croatia'},
 {name:'Rodrez', country:'Columbia'},
 {name:'Jermain', country:'Cuba'}
 ];
 });*/

/* #8 */
/*var app = angular.module('myApp', []);
 app.controller('myCtrl', function($scope, $http){
 $http.get("welcome.htm").then(function(response){
 $scope.myWelcome = response.data;
 });
 });*/

/* #9 */
/*var app = angular.module('myApp', []);
 app.controller('myCtrl', function($scope, $timeout){
 $scope.myHeader = "Hello World";
 $timeout(function(){
 $scope.myHeader = "How are you today?";
 }, 2500);
 });*/

/* #10 */
/*var app = angular.module('myApp', []);
 app.controller('myCtrl', function($scope, $interval){
 $scope.theTime = new Date().toLocaleTimeString();
 $interval(function(){
 $scope.theTime = new Date().toLocaleTimeString();
 }, 1000);
 });*/

/* #11 */
/*
 var app = angular.module('myApp', []);

 //  service는 한번만 호출되나봄
 app.service('hexafy', function(){
 this.myFunc = function(x){
 console.log(x + ', ' + x.toString(16));
 return x.toString(16);
 }
 });
 app.controller('myCtrl', function($scope, hexafy){
 window.addEventListener('keydown', function(){
 if($scope.decNum !== undefined && $scope.decNum !== null){
 $scope.hex = hexafy.myFunc($scope.decNum);
 }else{
 $scope.hex = 0;
 }
 });
 });*/

/* #13 */
/*
 //  filter는 매번 갱신되고 service는 한번 호출 되는 듯
 var app = angular.module('myApp', []);
 app.service('hexafy', function(){
 this.myFunc = function(x){
 return x.toString(16);
 }
 });
 app.filter('myFormat', ['hexafy', function(hexafy){
 return function (x) {
 if(x !== undefined && x !== null){
 return hexafy.myFunc(x);
 }else{
 return 0;
 }
 }
 }]);*/

/* #14 */
/*var app = angular.module('myApp', []);
 app.controller('myCtrl', function($scope, $http){
 $http.get('https://www.w3schools.com/angular/customers.php')
 .then(function(response){
 $scope.myData = response.data.records;
 });
 });*/

/* #15 */
/*
 var app = angular.module('myApp', []);
 app.controller('myCtrl', function($scope, $http){
 $http.get('https://www.w3schools.com/angular/customers.php')
 .then(function(response){
 $scope.names = response.data.records;
 });
 });*/

/* #16 */
/*
 var app = angular.module('myApp', []);
 app.controller('customersCtrl', function($scope, $http){
 $http.get('https://www.w3schools.com/angular/customers.php')
 .then(function (response) {
 $scope.names = response.data.records;
 });
 });*/

/* #19 */
/*
 var app = angular.module('myApp', []);
 app.controller('myCtrl', function($scope){
 $scope.count = 0;
 $scope.count1 = 0;
 $scope.count2 = 0;
 $scope.myFunction = function(){
 $scope.count2++;
 }
 });*/

/* #20 */
/*var app = angular.module('myApp', []);
 app.controller('myCtrl', function($scope){
 $scope.myFunc = function(){
 $scope.showMe = !$scope.showMe;
 }
 });*/

/* #21 */
/*var app = angular.module('myApp', []);
 app.controller('myCtrl', function($scope){
 $scope.myFunc = function(e){
 $scope.x = e.clientX;
 $scope.y = e.clientY;
 }
 });*/

/* #23 */
/*var app = angular.module('myApp', []);
 app.controller('myCtrl', function($scope){
 $scope.master = {firstName : "Canet", lastName : "Robern"};
 $scope.reset = function(){
 $scope.user = angular.copy($scope.master);
 };
 $scope.reset();
 });*/

/* #25 */
/*
 var app = angular.module('myApp', []);
 app.directive('myDirective', function(){
 return {
 require : 'ngModel',
 link : function(scope, element, attr, mCtrl){
 function myValidation(value){
 if(value.indexOf('e') > -1 ){
 mCtrl.$setValidity('charE', true);
 }else {
 mCtrl.$setValidity('charE', false);
 }
 return value;
 }
 mCtrl.$parsers.push(myValidation);
 }
 }
 });*/

/* #26 */
/*var app = angular.module('myApp', []);
 app.controller('validateCtrl', function($scope){
 //$scope.user = 'Canet';
 //$scope.email = 'akacanet87@gmail.com'
 });*/

/* #27 */
/*
 angular.module('myApp', [])
 .controller('userCtrl', function ($scope) {
 $scope.firstName = '';
 $scope.lastName = '';
 $scope.password = '';
 $scope.repeat = '';
 $scope.users = [
 {id: 1, firstName: 'Canet', lastName: 'Robern'},
 {id: 2, firstName: 'Neymar', lastName: 'daSilva'},
 {id: 3, firstName: 'Lionel', lastName: 'Messi'},
 {id: 4, firstName: 'David', lastName: 'BeckHam'},
 {id: 5, firstName: 'Wayne', lastName: 'Rooney'},
 {id: 6, firstName: 'Olivier', lastName: 'Giroud'},
 {id: 7, firstName: 'Rose', lastName: 'Mary'}
 ];
 $scope.edit = true;
 $scope.error = false;
 $scope.incomplete = false;
 $scope.hideForm = true;
 $scope.editUser = function (id) {
 $scope.hideForm = false;
 if (id === 'new') {
 $scope.edit = true;
 $scope.incomplete = true;
 $scope.firstName = '';
 $scope.lastName = '';
 } else {
 $scope.edit = false;
 $scope.firstName = $scope.users[id - 1].firstName;
 $scope.lastName = $scope.users[id - 1].lastName;
 }
 };

 $scope.$watch('password', function(){$scope.test();});
 $scope.$watch('repeat', function(){$scope.test();});
 $scope.$watch('firstName', function(){$scope.test();});
 $scope.$watch('lastName', function(){$scope.test();});

 $scope.test = function(){
 if($scope.password !== $scope.repeat){
 $scope.error = true;
 }else{
 $scope.error = false;
 }
 $scope.incomplete = false;
 if($scope.edit && (!$scope.firstName.length || !$scope.lastName.length || !$scope.repeat.length)){
 $scope.incomplete = true;
 }
 };

 });
 */

/* #28 */
/*
angular.module('animApp', [])
    .directive('myRect', ['$animate', function ($animate) {
        return function ($scope, element) {
            var rect = element[0].getBoundingClientRect();
            $scope.right = false;
            $scope.bottom = false;
            $scope.left = false;
            $scope.top = false;
            setInterval(function(){
                if (parseInt(rect.top) === 0 && parseInt(rect.left) === 0) {
                    //$animate.addClass(element, 'ng-anim-right');
                    $scope.right = true;
                    console.log(parseInt(rect.top) + ', ' + parseInt(rect.left));
                } else if (parseInt(rect.top) === 0 && parseInt(rect.left) === 700) {
                    $animate.removeClass(element, 'ng-anim-right').addClass(element, 'ng-anim-bottom');
                } else if (parseInt(rect.top) === 600 && parseInt(rect.left) === 700) {
                    $animate.removeClass(element, 'ng-anim-bottom').addClass(element, 'ng-anim-left');
                } else if (parseInt(rect.top) === 600 && parseInt(rect.left) === 0) {
                    $animate.removeClass(element, 'ng-anim-left').addClass(element, 'ng-anim-top');
                }
            }, 100);
        };
}]);*/


/* #29 */
/*
var app = angular.module('test', []);

app.directive('ngSlider', function() {
    return {
        scope: true,
        template: "<div class='ng-slider' ng-style='pos'></div>",
        replace: true,
        controller: function($scope, $interval) {

            $scope.pos = {
                top: 0,
                left: 0
            };

            $scope.newPos = function() {
                // calculate however you'd like:
                $scope.pos.top = Math.random() * 400 + "px";
                $scope.pos.left = Math.random() * 500 + "px";
            };

            $interval($scope.newPos, 1000);
        }
    };
});*/

/* #30 */
/*
var app = angular.module('myApp', ['ngRoute']);
app.config(function($routeProvider){
    $routeProvider
        .when('/', {
            //template : 'Main Page',       //  template을 추가하면 하위 templateUrl과 controller는 무시됨
            templateUrl : 'main.html',
            controller : 'homeCtrl'
        })
        .when('/red', {
            templateUrl : 'red.html',
            controller : 'redCtrl'
        })
        .when('/green', {
            templateUrl : 'green.html',
            controller : 'greenCtrl'
        })
        .when('/blue', {
            templateUrl : 'blue.html',
            controller : 'blueCtrl'
        });
});
app.controller('homeCtrl', function($scope){
    $scope.msg = 'Nothings in here';
});
app.controller('redCtrl', function($scope){
    $scope.msg = 'I love color red';
});
app.controller('greenCtrl', function($scope){
    $scope.msg = 'I love color green';
});
app.controller('blueCtrl', function($scope){
    $scope.msg = 'I love color blue';
});*/

/* #31 */
/*var app = angular.module('shoppingList', []);
app.controller('myCtrl', function($scope){
    $scope.products = ['Milk', 'Bread', 'Cheese'];
    $scope.addItem = function(){
        $scope.errortext = '';
        if(!$scope.addMe){
            return;
        }
        if($scope.products.indexOf($scope.addMe) === -1){
            $scope.products.push($scope.addMe);
        } else {
            $scope.errortext = 'The item is already in your shopping list.';
        }
        $scope.addMe = '';
    };
    $scope.removeItem = function(x){
        $scope.errortext = '';
        $scope.products.splice(x,1);
    };
});*/
